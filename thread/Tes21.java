package thread;

public class Tes21 {
    public static void main(String[] args) {
        new MyThread("高级", 10).start();
        new MyThread("低级", 1).start();
    }
}
/**
 * InnerTest1
 */
class MyThread extends Thread{
    public MyThread(String name, int pro){
        super(name);
        setPriority(pro);
    }
    @Override
    public void run(){
        for (int i = 0; i < 10000; i++) {
            System.out.println(this.getName()+"线程第"+i+"次执行");
        }
    }
    
}