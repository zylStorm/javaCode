<!--
 * @Author: your name
 * @Date: 2020-10-23 21:03:23
 * @LastEditTime: 2020-10-23 21:09:34
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\io\BIO\bio.md
-->
## 概述

BIO方法使用于连接数目比较小且固定的架构，这种方法对服务器资源要求比较高，并发局限于应用中，JDK1.4以前的唯一选择，但程序简单容易理解

Java BIO就是传统的java io编程，其相关的类和接口再java.io.BIO :同步阻塞，服务实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接接下来不做任何事情会造成不必要的线程开销，可以通过线程池的方法来改善（实现多个客户连接服务器）

![](https://img-blog.csdnimg.cn/20191122100241621.PNG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3pfeGluZG9uZw==,size_16,color_FFFFFF,t_70)