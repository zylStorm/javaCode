## 流式编程

流式编程让集合中的对象像水流一样流动，分别去进行去重、过滤、映射操作，就和批量化生产线一样。利用流，我们无需迭代集合中的元素，就可以提取和操作他们，这些操作通常被组合在一起，在流上形成一条操作管道。

> 流的一个好处就是它使得程序更加短小并且更易理解
---

### 1 创建流

* 将数组转换成流，可以产生不同基本数据类型的流，使用Arrays.Stream<T[] t> 方法
* 根据一组对象产生流，但不能产生基本数据类型的流，只能产生对应包装类的流
* 大部分集合都有stream()方法来产生对应的流
---

### 2 中间操作

中间操作具体包括去重、过滤、映射等操作，值得说明的是，在执行中间操作的代码的时候并不会执行这些操作，而只会把这些操作保存在流里面，每次中间操作都会产生一个新的流对象，保存从开始到现在要进行的所有操作序列，在执行结束操作的时候才会真正执行这些操作，这叫做懒加载。

* 跟踪和调试 peek()操作
* 排序 sorted()操作,传入一个Comparator参数
* 移除元素
    * distinct() 消除流中的重复元素，比创建set效率高
    * filter() 过滤操作则会留下使过滤器方法返回true的元素
* 应用函数到元素
    * map(Function) 将原来流中的每个元素都调用参数里的方法，其返回值汇总起来产生一个新的流。
    * mapToInt(ToIntFunction) 操作同上，但结果是IntStream。
    * mapToLong(ToLongFunction) 操作同上，结果是LongStream。
    * mapToDouble(toDoubleFunction) 操作同上，结果是DoubleStream

> 一个流只能进行一次操作
---

### 3 结束操作
这些操作接收一个流并产生一个最终结果，他们不会向后面的流提供任何东西，因此，结束操作总是你在管道的最后一个操作

* 转化为数组
    * toArray(): 将流转换成适当类型的数组。
    * toArray(): 在特殊情况下，生成器用于分配自定义的数组存储。
* 遍历元素
    * forEach(Conssumer): 使用System.out::print 作为Consumer函数,
    * forEachOrdered(Consumer): 确保按照原始顺序输出
    > 如果没有调用parallel()函数，两种输出一样
* 收集
    * collect(Collector)：使用collector收集元素到结果集合中
    * collect(Supplier,BiConsumer,BiConsumer):收集流元素到结果集合中，第一个参数用于创建一个新的结果集合，第二个参数用于将下一个元素加入到现有结果合集中，第三个参数用于将两个结果合集合并。
    
    第一种形式中的的Collector参数，Java核心库为我们提供了很多Collector实现类，都在Collectors这个工具类里面，例如Colletors.toList、Collectos.toMap、Collections.toCollection等等，基本上都是故名思义，就不介绍了。当然也可以自己写写一个类来继承自Collector来实现自定义的收集要求
* 匹配
    * allMatch(Predicate) ：如果流的每个元素根据提供的 Predicate 都返回 true 时，最终结果返回为 true。这个操作将会在第一个 false 之后短路，也就是不会在发生 false 之后继续执行计算。
    * anyMatch(Predicate)：如果流中的任意一个元素根据提供的 Predicate 返回 true 时，最终结果返回为 true。这个操作将会在第一个 true 之后短路，也就是不会在发生 true 之后继续执行计算。
    * noneMatch(Predicate)：如果流的每个元素根据提供的 Predicate 都返回 false 时，最终结果返回为 true。这个操作将会在第一个 true 之后短路，也就是不会在发生 true 之后继续执行计算。
* 元素查找
    * findFirst()：返回一个含有第一个流元素的 Optional类型的对象，如果流为空返回 Optional.empty。
    * findAny()：返回含有任意流元素的 Optional类型的对象，如果流为空返回 Optional.empty。
* 统计信息
    * average() ：求取流元素平均值。
    * max() 和 min()：求元素的最大值和最小值，对于非数字流则要多一个Comparator参数。
    * sum()：对所有流元素进行求和。
    * count()：流中的元素个数。
