/*
 * @Author: zyl
 * @Date: 2020-10-23 09:54:39
 * @LastEditTime: 2020-10-23 14:40:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\streamProgram\Prime.java
 */
package java8newPro.streamProgram;

import java.util.stream.LongStream;

public class Prime {

    public static boolean isPrime(long n) {
        return LongStream.rangeClosed(2, (long)Math.sqrt(n))
        .noneMatch(i -> n%i == 0);
        //如果流中所有元素调用上述方法都返回false，则nonMatch()返回true
    }

    public LongStream numbers(){
        return LongStream.iterate(2, i -> i+1)
        .filter(Prime::isPrime);
    }
    public static void main(String[] args) {
        new Prime().numbers().limit(10).forEach(n -> System.out.format("%d ", n));
        System.out.println();
        new Prime().numbers().skip(90).limit(10).forEach(n -> System.out.format("%d ", n));
        // 2 3 5 7 11 13 17 19 23 29
        // 467 479 487 491 499 503 509 521 523 541
    }
}
