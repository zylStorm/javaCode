/*
 * @Author: your name
 * @Date: 2020-10-22 21:47:38
 * @LastEditTime: 2020-10-23 09:43:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\StreamPro.java
 */
package java8newPro.streamProgram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamGenerate {
    public static void main(String[] args) {
        // 1 创建流
        
        // 产生从0到200的随机浮点数的流
        DoubleStream randStream = new Random().doubles(0, 200);

        // 将数组转换成流，可以产生基本数据类型的流
        // 如IntStream,floatStream,DoubleStream等，运行效率高
        DoubleStream arrayStream = Arrays.stream(new double[]{1,3,4,5,2,1.23});

        // 根据一组对象产生流，但不能产生基本数据类型的流，只能产生对应包装类的流
        Stream<String> stream  = Stream.of("happy","sad","bad","yes");

        // 产生从0到9的整数流
        IntStream rangeStream = IntStream.range(0, 10);

        // 以第一参数为种子，迭代产生后面啊的对象的流
        Stream<Integer> interatStream = Stream.iterate(0, i->2*i);

        // 大部分集合都有stream()方法产生对应的流
        Stream<String> collectionStream = new ArrayList<String>().stream();
        Stream<String> parallelStream = new ArrayList<String>().parallelStream();
        
    } 
}
