/*
 * @Author: your name
 * @Date: 2020-10-23 09:42:32
 * @LastEditTime: 2020-10-23 09:45:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\streamProgram\Peeking.java
 */
package java8newPro.streamProgram;

import java.util.stream.Stream;

public class Peeking {
    public static void main(String[] args) {
        Stream.of("will I eat the apple?".split(" "))
        .map(w -> w + " ")
        .peek(System.out::print)
        .map(String::toUpperCase)
        .peek(System.out::print)
        .map(String::toLowerCase)
        .forEach(System.out::print);
         // 结果：will WILL will I I i eat EAT eat the THE the apple? APPLE? apple?
        // 可以看出，流是对每个元素分别进行操作，且是一个一个做操作后输出，就和流水线一样

    }
}
