/*
 * @Author: your name
 * @Date: 2020-10-23 09:45:55
 * @LastEditTime: 2020-10-23 09:52:07
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\streamProgram\SortedComparator.java
 */
package java8newPro.streamProgram;

import java.util.*;
import java.util.stream.Stream;

public class SortedComparator {
    public static void main(String[] args) {
        // PS: FileToWords是一个可以将文本转换成单词的流
        // Stream.of(T[] t)只能是对象数组，不能是基本数据类型
        Stream.of("what I want just is a complete success".split(" "))
            .skip(2)
            .limit(5)
            .sorted(Comparator.naturalOrder())
            .map(w -> w+" ")
            .forEach(System.out::print);
    }
}
