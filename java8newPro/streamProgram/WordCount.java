/*
 * @Author: your name
 * @Date: 2020-10-23 10:44:32
 * @LastEditTime: 2020-10-23 21:02:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\java8newPro\streamProgram\WordCount.java
 */
package java8newPro.streamProgram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCount {
    public static void main(String[] args) throws IOException {
        String path = "java8newPro\\streamProgram\\word.txt";
        Stream<String> stream = Files.lines(Path.of(path));
        stream = stream.flatMap(x -> Arrays.stream(x.split(" ")));
        // Map<Object, Long> collect = stream.collect(Collectors.groupingBy(x -> x, Collectors.counting()));
        Map<String,Integer> collect = stream.collect(Collectors.toMap(x->x, x->1,(oldValue,newValue)->(oldValue+newValue)));
        
        System.out.println(collect.toString());

    }
    
}
