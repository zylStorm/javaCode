/*
 * @Author: zyl
 * @Date: 2020-10-23 09:30:59
 * @LastEditTime: 2020-10-23 09:32:06
 * @LastEditors: Please set LastEditors
 * @Description: 链表类
 * @FilePath: \javaCode\labuladong\ListNode.java
 */
package labuladong;

public class ListNode {
    int val;
    ListNode next;
    ListNode(){}
    ListNode(int val){
        this.val = val;
    }
}
