/*
 * @Author: zyl
 * @Date: 2020-10-22 10:13:06
 * @LastEditTime: 2020-10-22 16:45:20
 * @LastEditors: Please set LastEditors
 * @Description: 双检锁，采用双锁机制，安全且在多线程情况下能保持高性能
 * @FilePath: \javaCode\designMode\SingletonDemo\SingletonDemo02.java
 */
package designMode.SingletonDemo;


public class SingletonDemo02 {
    private volatile static SingletonDemo02 singleton;
    private SingletonDemo02(){}
    public static SingletonDemo02 getInstance(){
        if(singleton==null){
            synchronized(SingletonDemo02.class){
                if(singleton == null){
                    singleton = new SingletonDemo02();
                }
            }
        }
        return singleton;
    }
}
