package designMode.SingletonDemo;

// 饿汉式，线程安全
public class SingletonDemo {
    private static SingletonDemo instance = new SingletonDemo();
    private SingletonDemo(){}
    public static SingletonDemo getInstance(){
        return instance;
    }
}
