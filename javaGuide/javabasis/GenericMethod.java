/*
 * @Author: your name
 * @Date: 2020-10-22 19:49:43
 * @LastEditTime: 2020-10-22 19:54:11
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\GenericMethod.java
 */
package javaGuide.javabasis;


public class GenericMethod {

    // 泛型方法
    public static <E> void printArray(E[] inputArray){
        for(E element : inputArray){
            System.out.printf("%s ", element);
        }
        System.out.println();
    }
    public static void main(String[] args) {
        Integer[] intArray = {1,2,3};
        String[] stringArray = {"hello","world"};
        printArray(intArray);
        printArray(stringArray);
    }
}
