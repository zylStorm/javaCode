/*
 * @Author: your name
 * @Date: 2020-10-22 20:48:23
 * @LastEditTime: 2020-10-23 09:05:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\javabasis\ArrayToArrayList.java
 */
package javaGuide.javabasis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;



    
public class ArrayToArrayList {
    // 将数组转化为list,如果直接使用Arrays.asList(a)，则在使用List的add(),remove()方法时就会报错
    // 因为内部还是数组，我们可以使用泛型方法进行转换

    // 1 自己动手实现
    static <T> List<T> arrayToList(final T[] array){
        final List<T> l = new ArrayList<>(array.length);
        for(final T s : array){
            l.add(s);
        }
        return l;
    }

    
    public static void main(String[] args) {
        Integer[] myArray = {1,2,3};
        // 2 使用ArrayList<>的静态方法，最简单的方法
        List myList = new ArrayList<>(Arrays.asList(myArray));

        // 3 使用java8的集合流Stream
        List list2 = Stream.of(myArray).collect(Collectors.toList());
        // 基本数据类型也可以使用流，使用boxed的装箱操作    
        int[] array = {1,2,3,4}; 
        List list3 = Arrays.stream(array).boxed().collect(Collectors.toList());
        
        // 4 使用Guava（推荐）
        // 对于不可变集合，你可以十四用ImmutableList类及其of()和copyof()工厂方法（参数不能为空）
//         List<String> il = ImmutableList.of("string", "elements");  // from varargs
//         List<String> il = ImmutableList.copyOf(aStringArray);      // from array

        List<Integer> list4 = List.of(myArray);
        System.out.println(myList.getClass());    
    }
}
