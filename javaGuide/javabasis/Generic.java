/*
 * @Author: zyl
 * @Date: 2020-10-22 19:35:35
 * @LastEditTime: 2020-10-22 19:37:05
 * @LastEditors: Please set LastEditors
 * @Description: 泛型类
 * @FilePath: \javaCode\javaGuide\Generic.java
 */
package javaGuide.javabasis;


public class Generic<T> {
    private T key;
    public Generic(T key){
        this.key = key;
    }
    public T getKet(){
        return key;
    }
    public static void main(String[] args) {
        Generic<Integer> generic = new Generic<Integer>(123);
    }
}
