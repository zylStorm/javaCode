<!--
 * @Author: your name
 * @Date: 2020-10-22 20:21:03
 * @LastEditTime: 2020-10-22 20:45:19
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\javabasis\basis.md
-->
## 1 重载和重写的区别
> 重载就是同样的一个方法能够根据输入数据的不同，做出不同的处理
>
> 重写就是当子类继承自父类的相同方法，输入数据一样，但是要做出有别于父类的响应时，就要覆盖父类的方法

**重载**

发生在一个类中，编译期，方法名必须相同，参数类型必须不同、个数不同、顺序不同。方法返回值和访问修饰符可以不同。

综上：重载就是同一个类中多个同名方法根据不同的参数来执行不同的逻辑处理

**重写：**

重写发生在运行期，是子类对父类的允许访问你的方法的执行过程进行重新编写。

* 返回值类型、方法名、参数列表都必须相同，抛出的异常范围小于等于父类，访问修饰符法不能大于父类。
* 如果父类方法访问修饰符为private/final/static，则子类就不能重写该方法，但是被static修饰的方法能够再次声明
* 构造方法不可以被重写

综上：重写就是子类对父类方法的重新改造，外部样子不能改变，内部逻辑可以发生变化。

---

## 2 在java中定义一个无参构造器的作用

Java 程序在执行子类的构造方法之前，如果没有用 super()来调用父类特定的构造方法，则会调用父类中“没有参数的构造方法”。因此，如果父类中只定义了有参数的构造方法，而在子类的构造方法中又没有用 super()来调用父类中特定的构造方法，则编译时将发生错误，因为 Java 程序在父类中找不到没有参数的构造方法可供执行。解决办法是在父类里加上一个不做事且没有参数的构造方法。

---
## 3 接口和抽象类的区别
* 接口的方法默认是 public，所有方法在接口中不能有实现(Java 8 开始接口方法可以有默认实现），而抽象类可以有非抽象的方法。
* 接口中除了 static、final 变量，不能有其他变量，而抽象类中则不一定。
* 一个类可以实现多个接口，但只能实现一个抽象类。接口自己本身可以通过 extends 关键字扩展多个接口。
* 接口方法默认修饰符是 public，抽象方法可以有 public、protected 和 default 这些修饰符（抽象方法就是为了被重写所以不能使用 private 关键字修饰！）。
从设计层面来说，抽象是对类的抽象，是一种模板设计，而接口是对行为的抽象，是一种行为的规范。