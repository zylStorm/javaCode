/*
 * @Author: your name
 * @Date: 2020-10-27 15:56:55
 * @LastEditTime: 2020-10-27 16:00:33
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\javabasis\Person.java
 */
package javaGuide.javabasis;

import java.util.Collections;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class Person implements Comparable<Person>{
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        if(this.age>o.getAge()) return 1;
        if(this.age<o.getAge()) return -1;
        return 0;
    }
    public static void main(String[] args) {
        TreeMap<Person,String> pdata = new TreeMap<>();
        pdata.put(new Person("张三", 30), "zhangsan");
        pdata.put(new Person("李四", 20), "lisi");
        pdata.put(new Person("王五", 10), "wangwu");
        pdata.put(new Person("小红", 5), "xiaohong");
        Set<Person> keys = pdata.keySet();
        for(Person key : keys){
            System.out.println(key.getAge()+" "+key.getName());
        }
        ConcurrentHashMap

    }
    
}
