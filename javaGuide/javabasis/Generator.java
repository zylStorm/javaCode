package javaGuide.javabasis;


public interface Generator<T> {
    public T method();
}

// 实现泛型接口，未指定类型
class GeneratorImpl<T> implements Generator<T>{
    @Override
    public T method() {
        // TODO Auto-generated method stub
        return null;
    }
}

// 实现泛型接口，指定类型

class GeneratorImpll implements Generator<String>{
    @Override
    public String method() {
        // TODO Auto-generated method stub
        return "hello";
    }
}
