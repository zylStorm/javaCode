/*
 * @Author: your name
 * @Date: 2020-10-26 09:00:15
 * @LastEditTime: 2020-10-26 09:03:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\javabasis\CollectionToArray.java
 */
package javaGuide.javabasis;

import java.util.*;

public class CollectionToArray {
    public static void main(String[] args) {
        String[] s = {"dog","lazy","a","over","jumps","fox","brown"};
        List<String> list = Arrays.asList(s);
        Collections.reverse(list);
        s = list.toArray(new String[0]);
        System.out.println(Arrays.toString(s));
    }
}
