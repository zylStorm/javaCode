package javaGuide.reflect;

public class TargetObject {
    private String value;

    public TargetObject(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public void publicMethod(String s){
        System.out.println("I love "+s);
    }
    private void privateMethod(){
        System.out.println("value is "+value);
    }
}
