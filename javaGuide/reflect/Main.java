/*
 * @Author: your name
 * @Date: 2020-10-26 09:40:53
 * @LastEditTime: 2020-10-26 10:35:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\reflect\Main.java
 */
package javaGuide.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws Exception{
        Class<?> targetClass = Class.forName("javaGuide.reflect.TargetObject");
        TargetObject targetObject = (TargetObject)targetClass.newInstance();
        // 获取类中的所有方法
        Method[] methods = targetClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
        }
        // 获取指定的方法并调用
        Method publicMethod = targetClass.getDeclaredMethod("publicMethod", String.class);
        publicMethod.invoke(targetObject, "JavaGuide");
        Field field = targetClass.getField("value");
        field.setAccessible(true);
        field.set(targetObject,"JavaGuide");
        Method privateMethod = targetClass.getDeclaredMethod("privateMethod");
        privateMethod.setAccessible(true);
        privateMethod.invoke(targetObject);
    }
}
