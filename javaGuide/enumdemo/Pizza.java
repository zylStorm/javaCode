/*
 * @Author: your name
 * @Date: 2020-10-26 09:08:49
 * @LastEditTime: 2020-10-26 09:30:20
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\enumdemo\Pizza.java
 */
package javaGuide.enumdemo;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import jdk.jfr.Timestamp;

public class Pizza {
    private PizzaStatus status;
    public enum PizzaStatus {
        ORDERED(5){
            @Override
            public boolean isOrdered() {
                // TODO Auto-generated method stub
                return true;
            }
        },
        READY(2){
            @Override
            public boolean isReady() {
                // TODO Auto-generated method stub
                return true;
            }
        },
        DELIVERED(0){
            @Override
            public boolean isDelivered() {
                // TODO Auto-generated method stub
                return true;
            }
        };
    

        private int timeToDelivery;
        public boolean isOrdered(){
            return false;
        }
        public boolean isReady(){
            return false;
        }
        public boolean isDelivered(){
            return false;
        }
        public int getTimeToDelivery(){
            return timeToDelivery;
        } 

        PizzaStatus(int timeToDelivery){
            this.timeToDelivery = timeToDelivery;
        }
    }
    public boolean isDeliverable(){
        return this.status.isReady();
    }
    public void printTimeToDeliver(){
        System.out.println("Time to delivery is "+ this.getStatus().getTimeToDelivery());
    }

    public PizzaStatus getStatus() {
        return status;
    }

    public void setStatus(PizzaStatus status) {
        this.status = status;
    }
    
    @Test
    public void givenPizaOrder_whenReady_thenDeliverable(){
        Pizza testPz = new Pizza();
        testPz.setStatus(Pizza.PizzaStatus.READY);
        assertTrue(testPz.isDeliverable());
    }

}

