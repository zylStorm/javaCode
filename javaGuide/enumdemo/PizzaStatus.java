/*
 * @Author: your name
 * @Date: 2020-10-26 09:05:20
 * @LastEditTime: 2020-10-26 09:08:11
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \javaCode\javaGuide\enumdemo\PizzaStatus.java
 */
package javaGuide.enumdemo;

public enum PizzaStatus {
    ORDERED,
    READY,
    DELIVERED;
    public static void main(String[] args) {
        System.out.println(PizzaStatus.ORDERED.name());
        System.out.println(PizzaStatus.ORDERED);
        System.out.println(PizzaStatus.ORDERED.name().getClass());
        System.out.println(PizzaStatus.ORDERED.getClass());
    }
}
